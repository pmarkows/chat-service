package com.sda.chat;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FileNameGeneratorTest {

    @Test
    public void generate() {
        String UUID = new FileNameGenerator().generate();
        assertEquals(36, UUID.length());
        System.out.println(UUID);
    }

}