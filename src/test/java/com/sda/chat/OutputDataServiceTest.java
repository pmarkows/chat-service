package com.sda.chat;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class OutputDataServiceTest {

    public static final String FILENAME = "test";
    OutputDataService outputDataService = new OutputDataService();

    @Test
    void saveDataToFile() throws IOException {
        outputDataService.saveDataToFile(FILENAME, FILENAME);
        assertTrue(Files.exists(Paths.get(OutputDataService.CHAT_ROOM_FOLDER + "/" + FILENAME)));
    }

    @AfterEach
    public void deleteFile() throws IOException {
        Files.deleteIfExists(Paths.get(OutputDataService.CHAT_ROOM_FOLDER + "/" + FILENAME));
        Files.deleteIfExists(Paths.get(OutputDataService.CHAT_ROOM_FOLDER));
    }


}